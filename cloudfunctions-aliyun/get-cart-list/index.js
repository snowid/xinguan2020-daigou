/**
 * @description  获取购物车列表
 * @author unhejing (成都-敬宇杰)
 * @date 2020-02-21 上午20:00:07
 */
'use strict';
const db = uniCloud.database()
exports.main = async (event, context) => {
	//event为客户端上传的参数
	console.log('event:' + event)
	// todo user_id到时候从拦截器里获取
	event.user_id = "1"
	if (event.user_id == '' || !event.user_id) {
		return {
			success: false,
			code: -1,
			msg: '参数错误，缺少用户id'
		}
	}

	console.log('event.user_id:' + event.user_id)
	let res = await db.collection('cart').where({
		'user_id': event.user_id,
	}).orderBy("create_time","desc").get()

	if (!res.data || res.data.length === 0) {
		return {
			success: false,
			code: -1,
			msg: '暂无数据'
		}
	}
	console.log("购物车响应数据:" + JSON.stringify(res))
	let cartList = res.data;
	let productIds = new Array();
	cartList.map(o => productIds.push(o.product_id))
	console.log("获取商品相关信息：" + JSON.stringify(productIds))
	// 获取商品相关信息
	const dbCmd = db.command;
	let productRes = await db.collection('product').where({
		_id: dbCmd.in(productIds)
	}).get()
	let productList = productRes.data;
	console.log("productList:" + JSON.stringify(productList))
	let productMap = new Map();
	productList.map(o => {
		productMap.set(o._id, o);
	})
	console.log("productMap:" + JSON.stringify(productMap.size))
	// 设置购物车信息
	cartList = await cartList.map(o=>{
		let product = productMap.get(o.product_id);
		o.product_code = product.product_code
		o.product_name = product.product_name
		o.sale_price = product.sale_price
		o.book_price = product.book_price
		o.primary_img = product.primary_img
		o.publish_status = product.publish_status
		return o;
	})
	// 设置商品信息
	if (res.id || res.affectedDocs >= 1) {
		return {
			success: true,
			code: 0,
			msg: '成功',
			data: cartList
		}
	}
	return {
		success: false,
		code: -1,
		msg: '服务器内部错误'
	}
};

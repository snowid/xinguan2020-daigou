'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

function isEmpty(v) {
	switch (typeof v) {
		case 'undefined':
			return true;
		case 'string':
			if (v.replace(/(^[ \t\n\r]*)|([ \t\n\r]*$)/g, '').length == 0) return true;
			break;
		case 'boolean':
			if (!v) return true;
			break;
		case 'number':
			if (isNaN(v)) return true;
			break;
		case 'object':
			if (null === v || v.length === 0) return true;
			for (var i in v) {
				return false;
			}
			return true;
	}
	return false;
}
var stringUtil = {
	isEmpty
};

const {isEmpty: isEmpty$1} = stringUtil;
const db = uniCloud.database();
var main = async (event, context) => {
	//event为客户端上传的参数
	console.log('event:' + event);
	// event.id = "5e5ccfca5d654c004d984bd3"
	// event.publish_status = 0
	
	console.log("*********************更新商品上下架状态 start*********************");
	// 校验参数
	console.log("校验数据...");
	let checkRes = checkData(event);
	if(!checkRes.success){
		console.log("校验数据失败！！！！");
		return checkRes;
	}
	console.log("校验数据成功^_^");
	let res = await db.collection("product").doc(event.id).update({
		publish_status:parseInt(event.publish_status)
	});
	console.log("更新上下架状态返回数据："+JSON.stringify(res));
	if(res.affectedDocs > 0){
		return {
			success: true,
			code: 0,
			msg: '更新成功'
		}
	}
	console.log("*********************更新商品上下架状态 end*********************");
	return {
		success: false,
		code: -1,
		msg: '服务器内部错误'
	}
};

function checkData(event){
	if(isEmpty$1(event.id)){
		return {
			success: false,
			code: -1,
			msg: '商品id必传'
		}
	}
	
	if(isEmpty$1(event.publish_status)){
		return {
			success: false,
			code: -1,
			msg: '上下架状态必传'
		}
	}
	return {
		success: true,
		msg:"校验通过"
	}
}

var updateProductStatus = {
	main: main
};

exports.default = updateProductStatus;
exports.main = main;
